setwd("/home/matt/git-repos/webmap-toolkit/data/")
haffutils::dl_file("https://www2.census.gov/geo/tiger/GENZ2018/shp/cb_2018_us_county_500k.zip")

shp <- rgdal::readOGR("cb_2018_us_county_500k.shp")
save(shp, file = "counties.RData")
