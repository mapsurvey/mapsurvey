setwd("/home/matt/git-repos/webmap-toolkit/data/kde-example-data")

set.seed(1)
lng <- runif(n = 90, min = -94, max = -85)

set.seed(1)
lat <- runif(n = 90, min = 42, max = 49)

df <- data.frame(lng = lng, lat = lat)

for (i in 1:length(lng)) {
  name <- paste0(as.numeric(Sys.time())*100000, "-", sample(letters, 1), sample(letters, 1), sample(letters, 1))
  if (i == 1) {
    name.list <- name
  } else {
    name.list <- append(name.list, name)
  }
}

for (i in 1:nrow(df)) {
  print(df[i,])
  write.csv(df[i,1:2], paste0(name.list[i],".csv"), row.names = FALSE)
}
